package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{ BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter }
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.util.Try

object SomewhatStupidBot extends App {

  args.toList match {
    case hostName :: port :: botName :: botKey :: track :: password :: cars :: _ =>
      new SomewhatStupidBot(hostName, Integer.parseInt(port), botName, botKey, Some(track), Some(password), cars.toInt)
    case hostName :: port :: botName :: botKey :: rest =>
      new SomewhatStupidBot(hostName, Integer.parseInt(port), botName, botKey, rest.headOption)
    case _ => println("args missing")
  }
}

class SomewhatStupidBot(host: String, port: Int, botName: String, botKey: String, track: Option[String] = None, password: Option[String] = None, cars: Int = 1) {
  implicit val formats = new DefaultFormats {}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  play(init(track), Learning())
  
  private def init(track: Option[String]) = {
    if (track.isDefined) {
      send(MsgWrapper("joinRace", JoinRace(Join(botName, botKey), track, password, cars)))
      next[JoinRace]("joinRace")
    } else {
      send(MsgWrapper("join", Join(botName, botKey)))
      next[Join]("join")
    }
    
    val myCar = next[CarId]("yourCar")
    val init = next[GameInit]("gameInit")
    
    println(init.race.track.pieces.mkString("\r\n"))
    
    Situation(myCar, init.race.track)
  }
  
  private def next[T](msgType: String)(implicit mf: scala.reflect.Manifest[T]) = {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper(gotType, data, _, _) if gotType == msgType => {
          data.extract[T]
        }
        case MsgWrapper(gotType, _,_ ,_) => {
          throw new IllegalStateException(s"Expected $msgType from server, got $gotType instead")
        }
        case _ => {
          throw new IllegalStateException("Invalid input from server")
        }
      }
    } else {
      throw new IllegalStateException("No more input from server")
    }
  }

  @tailrec private def play(previous: Situation, learned: Learning) {
    val line = reader.readLine()
    if (line != null) {
      val (current, learnedMore) = readSituation(line, previous, learned)

      val decision = decide(previous, current, learnedMore)
      
      val done = doIt(current, decision)
      play(done, learnedMore)
    }
  }
  
  private def readSituation(line: String, previous: Situation, learned: Learning) = {
    Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", positionData ,_ , tick) => {
          val positions = positionData.extract[List[CarPosition]]
          val current = previous.newPositions(positions, tick)
          val learnedFromPositions = Try(learned.learnFrom(current)).getOrElse(learned)
          
          if (learnedFromPositions != learned) {
            println("Learned: "+ learnedFromPositions)
          }
          
          (current, learnedFromPositions)
        }
        case MsgWrapper("crash", crashed, _, _) => {
          val id = crashed.extract[CarId]
          val learnedFromCrash = Try(learned.learnFromCrash(previous, id)).getOrElse(learned)
          
          if (learnedFromCrash != learned) {
            println("Learned: "+ learnedFromCrash)
          }
          
          if (previous.mine(id)) {
            println(s"Crashed at speed ${previous.speed} on angle ${previous.position.angle}/${previous.position.radius}")
            (previous.crash, learnedFromCrash)
          } else {
            (previous, learnedFromCrash)
          }
        }
        case MsgWrapper("spawn", spawned, _, _) => {
          val id = spawned.extract[CarId]
          if (previous.mine(id)) {
            println("Spawned")
            (previous.spawn, learned)
          } else {
            (previous, learned)
          }
        }
        case MsgWrapper("turboAvailable", turbo, _ , _) => {
          val t = turbo.extract[TurboAvailable]
          println(s"Got Turbo $t")
          (previous.gotTurbo(t), learned)
        }
        
        case MsgWrapper("lapFinished", lap, _, _) => {
          val finish = lap.extract[LapFinished]
          if (previous.mine(finish.car)) {
            println(s"LAP: ${finish.lapTime.formattedLapTime}")
          }
          
          (previous, learned)
        }
        
        case MsgWrapper("gameStart", _, _, _) => {
          println("Received: gameStart")
          (previous.spawn, learned)
        }
        
        case MsgWrapper("gameEnd", end, _, _) => {
          val finish = end.extract[GameEnd]
          
          val myPos = finish.results.map(_.car).indexWhere(previous.mine) + 1
          val myBestLap = finish.bestLaps.find(lap => previous.mine(lap.car)).get.result
          
          println(s"## Checkered Flag ## $myPos. (${myBestLap.formattedLapTime})")
          
          (previous, learned)
        }
        
        case MsgWrapper(msgType, _ , _, _) => {
          println("Received: " + msgType)
          (previous, learned)
        }
      }
  }
  
  private def decide(previous: Situation, current: Situation, learned: Learning) = 
    DecisionMaking.decide(previous, current, learned)
  
  private def doIt(current: Situation, decision: Decision) = {
    decision match {
        case ct: ControlThrottle => {
          val throttle = ct.throttle(current.throttle)
          
          send(MsgWrapper("throttle", throttle))
          val done = current.doing(decision, Some(throttle))
          
          if (done.tick.map(_ % 10 == 0).getOrElse(false) || done.changedDecision) {
            println(s"$ct ($throttle), ${current.angle}, ${current.speed}, ${current.position}")
          }
          
          done
        }
        case TurboCharge => {
          println("Activating turbo")
          
          send(MsgWrapper("turbo", "Fasten your seatbelts"))
          current.doing(decision)
        }
        case ChangeLane(direction) => {
          println("Changing lane")
          
          send(MsgWrapper("switchLane", direction.toString))
          current.doing(decision)
        }
        case _ => {
          send(MsgWrapper("ping", null))
          current
        }
      }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Learning(maxAngle: Int = 50, deceleration: Double = 0.02, speedLimits: Map[Double, Double] = Map(1.0 -> 4, 0.45 -> 7, 0.22 -> 9, 0.11 -> 10)) {
  def speedLimit(steepness: Double) = if (steepness == 0) {
    Double.MaxValue
  } else {
    val (s, limit) = speedLimits.toList.sortBy { case (s, v) => Math.abs(s - steepness) }.head
    
    limit * (s / steepness)
  }
  
  def warnAngle = maxAngle / 2
  
  def learnFrom(current: Situation) = {
    val increases = for {
      (steepness, speeds) <- current.completedBends if (speeds.forall(_ > speedLimit(steepness)))
    } yield steepness
    
    increases.distinct.foldLeft(this)((l, steepness) => l.copy(speedLimits = l.increaseLimit(steepness)))
  }
  
  def learnFromCrash(current: Situation, crashed: CarId) = {
    val newDeceleration = if (didNotSlowDownEnough(current, crashed)) deceleration * 1.1 else deceleration
    val newSpeedLimits = if (didNotBreakSpeedLimit(current, crashed)) {
      val (_, steepness) = current.bend(crashed)
     
      reduceLimit(steepness) 
    } else 
      speedLimits
    
    copy(deceleration = newDeceleration, speedLimits = newSpeedLimits)
  }
  
  def didNotSlowDownEnough(current: Situation, crashed: CarId) =
    current.mine(crashed) && 
    current.onBend && 
    !current.didSomeoneBumpIntoUs &&
    current.speedWhenEnteredBend > speedLimit(current.currentBend._2) * 1.1
  
  
  def didNotBreakSpeedLimit(current: Situation, crashed: CarId) = 
    current.onBend(crashed) &&
    current.bendSpeeds(crashed).forall(_ < speedLimit(current.bend(crashed)._2) * 1.1)
  
  
  def reduceLimit(steepness: Double) = {
    val (s, limit) = speedLimits.toList.sortBy { case (s, v) => Math.abs(s - steepness) }.head
    speedLimits.updated(s, limit / 1.1)
  }
  
  def increaseLimit(steepness: Double) = {
    val (s, limit) = speedLimits.toList.sortBy { case (s, v) => Math.abs(s - steepness) }.head
    speedLimits.updated(s, limit * 1.05)
  }
}


