package hwo2014

import org.json4s.JValue
import org.json4s.DefaultFormats
import org.json4s.Extraction

case class GameInit(race: Race)
case class Race(track: RaceTrack, cars: List[RaceCar], raceSession: RaceSession)
case class RaceTrack(id: String, name: String, pieces: List[TrackPiece], lanes: List[TrackLane], startingPoint: Point)
case class TrackPiece(length: Option[Double], switch: Option[Boolean], radius: Option[Int], angle: Option[Double]) {
  def laneRadius(laneDistance: Int) = for {
    a <- angle
    r <- radius
    lr = r + (if (a > 0) -laneDistance else laneDistance)
  } yield lr
  
  def steepness(laneDistance: Int = 0) = for {
    r <- laneRadius(laneDistance)
    a <- angle
  } yield a / r
}
case class TrackLane(distanceFromCenter: Int, index: Int)
case class RaceCar(id: CarId, dimensions: CarDimensions)
case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double)
case class RaceSession(laps: Option[Int], maxLapTimeMs: Option[Int], quickRace: Option[Boolean], durationMs: Option[Int])
case class Point(position: Position, angle: Double)
case class Position(x: Double, y: Double)

case class CarPosition(id: CarId, angle: Double, piecePosition: CarPiecePosition)
case class CarPiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: CarLane, lap: Int)
case class CarLane(startLaneIndex: Int, endLaneIndex: Int)
case class CarId(name: String, color: String)

case class JoinRace(botId: Join, trackName: Option[String], password: Option[String], carCount: Int)
case class Join(name: String, key: String)

case class GameEnd(results: List[CarResult], bestLaps: List[CarLapTime])
case class CarResult(car: CarId, result: Result)
case class Result(laps: Option[Int], ticks: Option[Int], millis: Option[Int])
case class CarLapTime(car: CarId, result: LapTime)
case class LapTime(lap: Option[Int], ticks: Option[Int], millis: Option[Int]) {
  def formattedLapTime = {
    val maybeTime = for {
      ms <- millis
      s = ms / 1000
      fraction = ms % 1000
    } yield "%d.%03d".format(s, fraction)
    
    maybeTime.getOrElse("-")
  }
}

case class LapFinished(car: CarId, lapTime: LapTime, raceTime: Result, ranking: Ranking)
case class Ranking(overall: Int, fastestLap: Int)

case class Dnf(car: CarId, reason: String)

case class TurboAvailable(turboDurationMilliseconds: Double, turboDurationTicks: Double, turboFactor: Double)

case class MsgWrapper(msgType: String, data: JValue, gameId: Option[String], gameTick: Option[Int]) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats {}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data), None, None)
  }
}