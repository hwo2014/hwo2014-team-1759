package hwo2014

case class Situation(
    myCar: CarId,
    track: RaceTrack,
    history: History = History(),
    crashed: Boolean = false,
    turbo: Option[TurboAvailable] = None,
    activeTurbo: Option[(TurboAvailable, Int)] = None,
    tick: Option[Int] = None
) {
  def braking = history.braking

  def angle = history.myPositions.headOption.map(_.angle * bendDirection).getOrElse(0.0)
  def throttle = history.throttle
  def slipping = history.myPositions match {
    case CarPosition(_, current, _) :: CarPosition(_, previous, _) :: _ => (current - previous) * bendDirection
    case _ => 0
  }
  def bendDirection = if (position.angle.getOrElse(0.0) >= 0) 1 else -1
  def onTrack = !crashed
  def mine(id: CarId): Boolean = id.color == myCar.color
  def mine(pos: CarPosition): Boolean = mine(pos.id)
  
  def crash = copy(crashed = true)
  def spawn = copy(crashed = false)
  def gotTurbo(t: TurboAvailable) = copy(turbo = if (onTrack) Some(t) else turbo)
  
  def newPositions(positions: List[CarPosition], now: Option[Int]) = {
    val myNewPosition = positions.find(mine)
    
    copy(history = history.newPositions(myNewPosition, positions), tick = now)
  }
  
  def doing(decision: Decision, throttle: Option[Double] = None) = {
    val (turboLeft, turboActive) = 
      if (decision == TurboCharge) 
        (None, Some((turbo.get, tick.get))) 
      else 
        (turbo, activeTurbo)
    
    copy(
        history = history.doing(decision, throttle),
        turbo = turboLeft,
        activeTurbo = turboActive
    )
  }
  
  def onStraight = history.myPositions.headOption.
    map(pos => track.pieces(pos.piecePosition.pieceIndex)).
    map(_.length.isDefined).
    getOrElse(true)
  
  def onBend = !onStraight
  def onBend(car: CarId) = {
    val carPositions = positions(car)
    val lastPosition = carPositions.headOption
    
    lastPosition.
      map(pos => track.pieces(pos.piecePosition.pieceIndex)).
      map(_.angle.isDefined).
      getOrElse(false)
  }
  def bendSpeeds(car: CarId) = {
    val carPositions = positions(car)
    val lastPosition = carPositions.head
    val currentPiece = track.pieces(lastPosition.piecePosition.pieceIndex)
    
    val onBend = carPositions.takeWhile(pos => track.pieces(pos.piecePosition.pieceIndex).steepness() == currentPiece.steepness())
    val inOrder = onBend.reverse 
    inOrder.zip(inOrder.tail).map(pos => speed(pos._1, pos._2))
  }
  def bend(car: CarId) = {
    val piece = track.pieces(lastPosition(car).piecePosition.pieceIndex)
    (0, piece.steepness(laneDistance(lane(car))).getOrElse(0.0).abs)
  }
    
  def positions(car: CarId) = history.otherPositions.map(_.find(_.id == car)).filter(_.isDefined).map(_.get)
  def lastPosition(car: CarId) = positions(car).head
  
  def speed: Double = history.myPositions match {
    case current :: previous :: _ => speed(previous, current)
    case _ => 0
  }
  def speed(car: CarId): Double = {
    val positions = history.otherPositions.map(poss => poss.find(_.id == car)).filter(_.isDefined).map(_.get)
    positions match {
      case current :: previous :: _ => speed(previous, current)
      case _ => 0
    }
  }
  
  def speed(pos1: CarPosition, pos2: CarPosition) = {
    val piece2 = track.pieces(pos2.piecePosition.pieceIndex)
    val piece1 = track.pieces(pos1.piecePosition.pieceIndex)
    
    if (pos2.piecePosition.pieceIndex == pos1.piecePosition.pieceIndex) {
      pos2.piecePosition.inPieceDistance - pos1.piecePosition.inPieceDistance
    } else {
      val l = length(piece1, pos1.piecePosition.lane.endLaneIndex)
      val travelledOnPreviousPiece = l - pos1.piecePosition.inPieceDistance
      
      pos2.piecePosition.inPieceDistance + travelledOnPreviousPiece
    }
  }
  
  def position = track.pieces(history.myPositions.
        headOption.
        map(_.piecePosition.pieceIndex).
        getOrElse(0))
  
  def nextBend = {
    val (currentPiece, distanceTravelled) = history.myPositions.
        headOption.
        map(pos => (pos.piecePosition.pieceIndex, pos.piecePosition.inPieceDistance)).
        getOrElse((0, 0.0))
    
    val (nextBend, bendIdx) = trackStream.
      zipWithIndex.
      drop(currentPiece).
      dropWhile(_._1.steepness() == position.steepness()).
      dropWhile(_._1.length.isDefined).
      head
    
    val piecesBetween = trackStream.drop(currentPiece + 1).take(bendIdx - currentPiece - 1)
    val distanceToBend = length(currentPiece, lane) - distanceTravelled + piecesBetween.map(pcs => length(pcs, lane)).sum
    
    val distanceToLane = laneDistance(lane)
    (distanceToBend, nextBend.steepness(distanceToLane).getOrElse(0.0).abs)
  }
  def currentBend = (0, position.steepness(laneDistance(lane)).getOrElse(0.0).abs)
  
  def length(index: Int, lane: Int): Double = length(track.pieces(index), lane)
  def length(piece: TrackPiece, lane: Int): Double = piece.length.getOrElse {
    val distanceToLane = laneDistance(lane)
    Math.abs(piece.angle.get) / 360.0 * 2 * Math.PI * piece.laneRadius(distanceToLane).get
  }
  def lane = history.myPositions.headOption.map(_.piecePosition.lane.endLaneIndex).getOrElse(0)
  def lane(car: CarId) = positions(car).headOption.map(_.piecePosition.lane.endLaneIndex).getOrElse(0)
  def laneDistance(laneIndex: Int) = 
    track.lanes.find(_.index == laneIndex).
      map(_.distanceFromCenter).
      getOrElse(0)
      
  def trackStream = Stream.continually(track.pieces).flatten
  def trackStreamReverse = Stream.continually(track.pieces.reverse).flatten
  
  def changedDecision = history.changedDecision
  
  def straightLength = {
    val straight = for {
      current <- history.myPositions.headOption
      position = current.piecePosition
      piece = track.pieces(position.pieceIndex)
      straight = trackStream.drop(position.pieceIndex).takeWhile(_.length.isDefined)
    } yield if (piece.length.isDefined) piece.length.get - position.inPieceDistance + straight.map(_.length.get).sum else 0
    
    straight.getOrElse(0.0)
  }
  
  def maxStraightLength = {
    val straightPieces = track.pieces.zipWithIndex.filter(_._1.length.isDefined)
    val straightLengths = for {
      (piece, idx) <- straightPieces
      straight = trackStream.drop(idx).takeWhile(_.length.isDefined)
    } yield piece.length.get + straight.map(_.length.get).sum
    
    straightLengths.max
  }
  
  def enteredBeforeSwitch = history.myPositions match {
    case current :: previous :: _ 
      if current.piecePosition.pieceIndex != previous.piecePosition.pieceIndex => 
        trackStream.drop(current.piecePosition.pieceIndex + 1).head.switch.isDefined
    case _ => false
  }
  
  def switchInterval = 
    if (track.pieces.count(_.switch.isDefined) == 0) {
      List((lane, 0.0))
    } else {
      val lanes = List(lane, lane - 1, lane + 1).filter(_ >= 0).filter(_ <= track.lanes.map(_.index).max)
      val ahead = trackStream.drop(history.myPositions.headOption.map(_.piecePosition.pieceIndex).getOrElse(0) + 1)
      val switchInterval = ahead.dropWhile(_.switch.isEmpty).drop(1).takeWhile(_.switch.isEmpty)
      
      for {
        l <- lanes
        laneLength = switchInterval.map(pc => length(pc, l)).sum
      } yield (l, laneLength)
    }
  
  def speedWhenEnteredBend = {
    val currentPiece = position
    val onBend = history.myPositions.takeWhile(pos => track.pieces(pos.piecePosition.pieceIndex).steepness() == currentPiece.steepness())
    onBend.reverse match {
      case first :: second :: _ => speed(first, second)
      case _ => 0
    }
  }
  
  def didSomeoneBumpIntoUs = {
    val detected = for {
      all <- history.otherPositions
      me = all.find(mine)
      others = all.filterNot(mine)
      other <- others if (me.isDefined && other.piecePosition.pieceIndex == me.get.piecePosition.pieceIndex)
    } yield me.get.piecePosition.inPieceDistance - other.piecePosition.inPieceDistance < 50
    
    detected.fold(false)(_ | _)
  }
  
  def completedBends = history.otherPositions match {
    case current :: previous :: _ => {
      val nowOnStraight = current.filter(pos => track.pieces(pos.piecePosition.pieceIndex).length.isDefined).map(_.id)
      val previousOnBend = current.filter(pos => track.pieces(pos.piecePosition.pieceIndex).angle.isDefined).map(_.id)
      
      val previous = copy(history = history.copy(otherPositions = history.otherPositions.tail))
      val carsCompletedBend = nowOnStraight.filter(previousOnBend.contains)
      
      carsCompletedBend.map(previous.bend).map(_._2) zip carsCompletedBend.map(previous.bendSpeeds)
    }
    case _ => Nil
  }
  
  def amIFirst = history.first.map(mine).getOrElse(true)
  
  def distanceToNext = {
    val fromMe = history.inTrackOrder.filter(_.piecePosition.lane.endLaneIndex == lane).dropWhile(pos => !mine(pos))
    fromMe match {
      case me :: next :: _ if (me.piecePosition.pieceIndex == next.piecePosition.pieceIndex) => 
        next.piecePosition.inPieceDistance - me.piecePosition.inPieceDistance
      case me :: next :: _ => {
        val between = track.pieces.slice(me.piecePosition.pieceIndex, next.piecePosition.pieceIndex)
        
        between.map(pc => length(pc, lane)).sum - me.piecePosition.inPieceDistance + next.piecePosition.inPieceDistance
        
      }
      case _ => Double.MaxValue
    }
  }
  
  def tailing = {
    val untilMe = history.inTrackOrder.filter(_.piecePosition.lane.endLaneIndex == lane).reverse.dropWhile(pos => !mine(pos))
    untilMe match {
      case me :: tailing :: _ if (me.piecePosition.pieceIndex == tailing.piecePosition.pieceIndex) => 
        (me.piecePosition.inPieceDistance - tailing.piecePosition.inPieceDistance, speed(tailing.id))
      case me :: tailing :: _ => {
        val between = track.pieces.slice(tailing.piecePosition.pieceIndex, me.piecePosition.pieceIndex)
        
        val distance = between.map(pc => length(pc, lane)).sum - tailing.piecePosition.inPieceDistance + me.piecePosition.inPieceDistance
        (distance, speed(tailing.id))
      }
      case _ => (Double.MaxValue, 0.0)
    }
  }
}

case class History(
    decisions: List[Decision] = Nil,
    throttles: List[Double] = Nil, 
    myPositions: List[CarPosition] = Nil, 
    otherPositions: List[List[CarPosition]] = Nil
) {
  private val maxHistory = 60
  
  def braking = throttles match {
    case previous :: beforeThat :: _ => previous < beforeThat
    case _ => false
  }
  
  def throttle = throttles.headOption.getOrElse(0.0)
  
  def newPositions(myNewPosition: Option[CarPosition], allNewPositions: List[CarPosition]) = myNewPosition match {
    case Some(pos) => copy(
        myPositions = pos :: myPositions.take(maxHistory), 
        otherPositions = allNewPositions :: otherPositions.take(maxHistory)
    )
    case None => copy(otherPositions = allNewPositions :: otherPositions.take(maxHistory))
  }
  
  def doing(decision: Decision, throttle: Option[Double] = None) = {
    val newDecisionHistory = decision :: decisions.take(maxHistory)
    val newThrottleHistory = throttle.map(_ :: throttles.take(maxHistory)).getOrElse(throttles)
    
    copy(
        decisions = newDecisionHistory,
        throttles = newThrottleHistory
    )
  }
  
  def changedDecision = decisions match {
    case current :: previous :: _ => current != previous
    case _ => true
  }
  
  def inTrackOrder = otherPositions.headOption.
      map(_.sortBy { carPos => 
        val pos = carPos.piecePosition
        (pos.pieceIndex, pos.inPieceDistance)
      }).
      getOrElse(Nil)
  
  def first = otherPositions.headOption.
      map(_.maxBy { carPos => 
        val pos = carPos.piecePosition
        (pos.lap, pos.pieceIndex, pos.inPieceDistance)
      }).
      map(_.id)
}