package hwo2014

sealed trait Decision
sealed trait ControlThrottle extends Decision {
  val throttlingFactor = 1.1
  def throttle(previous: Double): Double
}
case object FullBrake extends ControlThrottle {
  def throttle(previous: Double) = 0
}
case object PedalToTheMetal extends ControlThrottle {
  def throttle(previous: Double) = 1
}
case object EmergencyBrake extends ControlThrottle {
  def throttle(previous: Double) = 0
}
case object Accelerate extends ControlThrottle {
  def throttle(previous: Double) = Math.max(Math.min(previous * throttlingFactor, 1.0), 0.1)
}
case object Decelerate extends ControlThrottle {
  def throttle(previous: Double) = previous / throttlingFactor
}
object Cruise {
  def withSpeed(target: Double) = Cruise(Math.min(target / 10, 1.0))
}
case class Cruise(throttle: Double) extends ControlThrottle {
  def throttle(previous: Double) = throttle
}
case object TurboCharge extends Decision

object ChangeLane {
  val left = ChangeLane(Left)
  val right = ChangeLane(Right)
}
case class ChangeLane(direction: Direction) extends Decision

case object Wait extends Decision

sealed trait Direction
case object Left extends Direction
case object Right extends Direction