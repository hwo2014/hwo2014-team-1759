package hwo2014

object DecisionMaking {
  val strategies = List(
      Rammturbostein,
      Rammstein,
      BrakeBeforeTailingRams,
      TurboChargeOnLongestStraight,
      SwitchToShortestLane,
      CruiseIntoBends,
      BrakeBeforeBend,
      FullThrottleOnStraight,
      BrakeWhenSlipping,
      DecelarateBeforeSlipping,
      CruiseThroughBends,
      JustAccelerate
  )
  
  def decide(previous: Situation, current: Situation, learned: Learning): Decision = 
    if (current.onTrack && current != previous)
      strategies.toStream.
        map(_.decide(current, learned)).
        dropWhile(_.isEmpty).
        head.get
    else
      Wait
}

sealed trait Strategy {
  def decide(current: Situation, learned: Learning): Option[Decision] = 
    if (apply(current, learned))
      Some(decision(current, learned))
    else
      None
  
  
  def apply(current: Situation, learned: Learning): Boolean
  def decision(current: Situation, learned: Learning): Decision
}

object Rammturbostein extends Strategy {
  def apply(current: Situation, learned: Learning) = 
    current.turbo.isDefined &&
    current.onStraight &&
    current.straightLength > 6 * current.distanceToNext &&
    current.distanceToNext < 120
  
  def decision(current: Situation, learned: Learning) = TurboCharge
}

object Rammstein extends Strategy {
  def apply(current: Situation, learned: Learning) = 
    current.onStraight &&
    current.distanceToNext / current.straightLength < 0.3
  
  def decision(current: Situation, learned: Learning) = PedalToTheMetal
}

object BrakeBeforeTailingRams extends Strategy {
  def apply(current: Situation, learned: Learning) = {
    val (distance, steepness) = current.nextBend
    
    val (distanceToTailing, speed) = current.tailing
    current.onStraight && 
      distanceToTailing < 120 &&
      speed > 1.1 * current.speed
      current.speed - distance * learned.deceleration / 2 > learned.speedLimit(steepness)
  }
    
  
  def decision(current: Situation, learned: Learning) = FullBrake
}

object TurboChargeOnLongestStraight extends Strategy {
  def apply(current: Situation, learned: Learning) = 
    current.turbo.isDefined && 
    (current.amIFirst || current.distanceToNext > 120) && 
    current.maxStraightLength - current.straightLength < 30 && 
    Math.abs(current.angle) < learned.maxAngle
  
  def decision(current: Situation, learned: Learning) = TurboCharge
}

object SwitchToShortestLane extends Strategy {
  def apply(current: Situation, learned: Learning) = 
    current.enteredBeforeSwitch && shortestLane(current) != current.lane
  
    def decision(current: Situation, learned: Learning) = 
      if (shortestLane(current) == current.lane - 1)
        ChangeLane.left
      else
        ChangeLane.right
        
  
  private def shortestLane(current: Situation) = {
    val lengths = current.switchInterval
    lengths.sortBy { case (_, l) => l }.head._1
  }
}

object BrakeBeforeBend extends Strategy {
  def apply(current: Situation, learned: Learning) = {
    val (distance, steepness) = current.nextBend
    current.speed - distance * learned.deceleration > learned.speedLimit(steepness)
  }
  
  def decision(current: Situation, learned: Learning) = FullBrake
}

object CruiseIntoBends extends Strategy {
  def apply(current: Situation, learned: Learning) = {
    val (distance, steepness) = current.nextBend
    val limit = learned.speedLimit(steepness)
    distance < 50 && 0.9 * limit < current.speed && current.speed < limit * 1.1
  }
  
  def decision(current: Situation, learned: Learning) = Cruise.withSpeed(learned.speedLimit(current.nextBend._2))
}

object FullThrottleOnStraight extends Strategy {
  def apply(current: Situation, learned: Learning) = 
    current.onStraight && Math.abs(current.angle) < learned.maxAngle
  
    def decision(current: Situation, learned: Learning) = PedalToTheMetal
}
object BrakeWhenSlipping extends Strategy {
  def apply(current: Situation, learned: Learning) = 
    (Math.abs(current.angle) > learned.maxAngle && current.slipping > 0) || 
    current.slipping > 5 || 
    (Math.abs(current.angle) > learned.warnAngle && current.slipping > 2.5) 
  
    def decision(current: Situation, learned: Learning) = EmergencyBrake
}

object DecelarateBeforeSlipping extends Strategy {
  def apply(current: Situation, learned: Learning) = {
    val (_, steepness) = current.currentBend
    val limit = learned.speedLimit(steepness)
    
    current.speed > limit &&
      ((current.angle > learned.warnAngle && current.slipping > 0) || 
      (current.angle > 0 && current.slipping > 1.5) ||
      current.slipping > 3)
  }
  
    def decision(current: Situation, learned: Learning) = Decelerate
}

object CruiseThroughBends extends Strategy {
  def apply(current: Situation, learned: Learning) = {
    val (_, steepness) = current.currentBend
    val limit = learned.speedLimit(steepness)
    current.throttle * 10 < limit || current.speed > limit * 1.2
  }
  
  def decision(current: Situation, learned: Learning) = Cruise.withSpeed(learned.speedLimit(current.currentBend._2))
}

object JustAccelerate extends Strategy {
  def apply(current: Situation, learned: Learning) = true
  def decision(current: Situation, learned: Learning) = Accelerate
}